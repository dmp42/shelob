# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
from pip.req import parse_requirements
import sys

__version__ = '0.1.0'

install_reqs = parse_requirements('requirements.txt')
reqs = [str(ir.req) for ir in install_reqs]
dependencies = [str(ir.url) for ir in install_reqs]

_ver = sys.version_info

if _ver[0] == 2 and _ver[1] <= 6:
    reqs.insert(0, 'simplejson')

setup(
    name='shelob',
    license='MIT',
    version=__version__,
    description='A small REST web rawler',
    author='Sébastien Eustace',
    author_email='sebastien.eustace@gmail.com',
    url='https://bitbucket.org/sdispater/shelob',
    setup_requires=['flake8'],
    packages=find_packages(),
    install_requires=reqs,
    tests_require=['nose', 'fakeredis'],
    test_suite='nose.collector',
    dependency_links=dependencies
)
