# -*- coding: utf-8 -*-

from ..compat import urlopen, urlparse, urljoin
from bs4 import BeautifulSoup


def crawl_urls(urls):
    images = []

    for url in urls:
        images += _crawl_url(url)

    return images


def _crawl_url(url):
    soup = BeautifulSoup(urlopen(url).read())
    images = []

    for i in soup.findAll('img'):
        if not _is_absolute_url(i['src']):
            images.append(urljoin(url, i['src']))
        else:
            images.append(i['src'])

    return images


def _is_absolute_url(url):
    return bool(urlparse(url).scheme)
