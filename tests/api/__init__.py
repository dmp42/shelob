# -*- coding: utf-8 -*-

from shelob import shelob

from tests import ShelobTestCase


class ApiTestCase(ShelobTestCase):

    def setUp(self):
        super(ApiTestCase, self).setUp()

        self.app = shelob.app.test_client()

    def assert_api_reponse(self, response, status=200):
        self.assertTrue('application/json' in response.headers['Content-Type'])
        self.assertEqual(response.status_code, status)
