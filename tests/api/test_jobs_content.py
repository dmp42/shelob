# -*- coding: utf-8 -*-

import requests

from rq import Queue
from shelob import shelob
from . import ApiTestCase

URL = 'http://www.sebastien-eustace.fr/images/photo-sebastien-eustace.jpg'
URL2 = 'http://ppcdn.500px.org/49576612/3055849e0309c64f707b66b9a61cb3913c54020e/2048.jpg'


def one_url():
    return [URL]


class TestJobsContent(ApiTestCase):

    def test_valid_response(self):
        """
        /jobs/content should return the requested image
        """
        q = Queue('low', connection=shelob.app.config['REDIS'], async=False)
        job = q.enqueue(one_url)

        response = self.app.get('/jobs/%s/content/%s' % (job.id, URL))
        direct_response = requests.get(URL)

        self.assertEqual(
            response.headers['Content-Type'], direct_response.headers['Content-Type'])
        self.assertEqual(
            response.headers['Content-Length'], direct_response.headers['Content-Length'])

    def test_no_job(self):
        """
        /jobs/content should return a 404 when no job could be found
        """
        # No job found
        response = self.app.get('/jobs/not_an_id/status')

        self.assert_api_reponse(response, 404)

    def test_no_image_in_job_result(self):
        """
        /jobs/content should return a 404 when the requested url is not in job's result
        """
        # No job found
        q = Queue('low', connection=shelob.app.config['REDIS'], async=False)
        job = q.enqueue(one_url)

        response = self.app.get('/jobs/%s/content/%s' % (job.id, URL2))

        self.assert_api_reponse(response, 404)
