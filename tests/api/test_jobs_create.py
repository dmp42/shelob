# -*- coding: utf-8 -*-

import json

from . import ApiTestCase
from shelob import shelob


class TestJobsCreate(ApiTestCase):

    def test_valid_response(self):
        """
        /jobs/create should return a job id
        """
        response = self.app.post('/jobs/create',
                                 data="http://google.com\nhttp://yahoo.com")

        self.assert_api_reponse(response)

        data = json.loads(response.data.decode("utf8"))
        self.assertTrue('id' in data)

    def test_accept_octet_stream(self):
        """
        /jobs/create should accept data as application/octet-stream
        """
        response = self.app.post('/jobs/create',
                                 data="http://google.com\nhttp://yahoo.com",
                                 headers=[('Content-Type', 'application/octet-stream')])

        self.assert_api_reponse(response)

        data = json.loads(response.data.decode("utf8"))
        self.assertTrue('id' in data)

    def test_accept_form_like_params(self):
        """
        /jobs/create should accept data as application/x-www-form-urlencoded
        """
        response = self.app.post('/jobs/create',
                                 data={'urls': ['http://google.com', 'http://yahoo.com']})

        self.assert_api_reponse(response)

        data = json.loads(response.data.decode("utf8"))
        self.assertTrue('id' in data)

    def test_keys_increment(self):
        """
        /jobs/create should add a key to redis
        """
        initial_keys = shelob.app.config['REDIS'].keys('rq:job:*')

        response = self.app.post('/jobs/create',
                                 data="http://google.com\nhttp://yahoo.com")

        self.assert_api_reponse(response)

        self.assertEqual(len(initial_keys) + 1, len(
            shelob.app.config['REDIS'].keys('rq:job:*')))

    def test_invalid_response(self):
        """
        /jobs/create should return a 422 error if input payload is invalid
        """
        # Empty payload
        response = self.app.post('/jobs/create')

        self.assert_api_reponse(response, 422)

        data = json.loads(response.data.decode("utf8"))
        self.assertEqual(data['message'], 'Empty payload')

        # Invalid payload
        response = self.app.post('/jobs/create',
                                 data='http://google')

        self.assert_api_reponse(response, 422)

        data = json.loads(response.data.decode("utf8"))
        self.assertEqual(data['message'], 'url "http://google" is invalid')
